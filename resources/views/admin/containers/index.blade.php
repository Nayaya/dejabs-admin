@extends('layouts.app');

@section('content')

<div class="col-lg-12 col-lg-offset-1">
    <div class="card-header" style="background: green; color: white">ALL CONTAINERS</div>
    <div class="card-body">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>NO.</th>
                    <th>BL NO.</th>
                    <th>SIZE</th>
                    <th>OWNER</th>
                    <th>STATUS</th>
                    <th>ETA</th>
                    <th>LINE</th>
                    <th>DATE RECEIVED</th>
                    <td colspan="2">ACTIONS</td>
                </tr>
            </thead>

            <tbody>
                @if (!empty($containers))
                    @foreach ($containers as $container)
                    <tr>

                        <td>{{ $container->container_no }}</td>
                        <td>{{ $container->bl_no }}</td>
                        <td>{{ $container->size }}</td>
                        <td>{{ $container->owner }}</td>
                        <td>{{ $container->status }}</td>
                        <td>{{ $container->eta }}</td>
                        <td>{{ $container->line }}</td>
                        <td>{{ $container->date_received }}</td>
                        <td>
                        @can('container-edit')
                            <a href="{{ route('containers.edit', $container->container_no)}}" class="btn btn-primary">Edit</a>
                        @endcan
                        </td>
                        <td>
                            <form action="{{ route('containers.destroy', $container->container_no)}}" method="post">
                                @csrf
                                @method('DELETE')
                                @can('container-delete')
                                 <button class="btn btn-danger" type="submit">Delete</button>
                                @endcan
                            </form>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td>
                            <div>No Records at this time</div>
                        </td>
                    </td>
                @endif
            </tbody>

        </table>
    </div>
    </div>
    @can('container-create')
    <a href="{{ route('containers.create') }}" class="btn btn-success">Add Container</a>
    @endcan
</div>

@stop
