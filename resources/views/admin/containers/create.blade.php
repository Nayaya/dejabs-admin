@extends('layouts.app');

@section('content')

<div class="card">
    <div class="card-header">Add a New Container</div>

    <div class="card-body">
        <form method="POST" action="{{ route('containers.store') }}">
                @csrf
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">BL No.</label>
                <div class="col-md-6">
                    <input id="bl_no" type="text" class="form-control" name="bl_no" value="{{ old('bl_no') }}" required>

                    @error('bl_no')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">Container No.</label>

                <div class="col-md-6">
                    <input id="container_no" type="text" class="form-control @error('container_no') is-invalid @enderror" name="container_no" value="{{ old('container_no') }}" required autocomplete="container_no">

                    @error('container_no')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="size" class="col-md-4 col-form-label text-md-right">Size</label>

                <div class="col-md-6">
                    <input id="size" type="text" class="form-control @error('size') is-invalid @enderror" name="size" required autocomplete="size">

                    @error('size')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="owner" class="col-md-4 col-form-label text-md-right">Owner</label>

                <div class="col-md-6">
                    <input id="owner" type="text" class="form-control" name="owner" required autocomplete="owner">

                    @error('owner')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="line" class="col-md-4 col-form-label text-md-right">Line</label>

                <div class="col-md-6">
                    <input id="line" type="text" class="form-control" name="line" required autocomplete="line">

                    @error('line')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="items" class="col-md-4 col-form-label text-md-right">Item (s)</label>

                <div class="col-md-6">
                    <input id="items" type="text" class="form-control" name="items" required autocomplete="items">

                    @error('items')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="status" class="col-md-4 col-form-label text-md-right">Status</label>

                <div class="col-md-6">
                    <input id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" required autocomplete="status">

                    @error('status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="eta" class="col-md-4 col-form-label text-md-right">ETA</label>

                <div class="col-md-6">
                    <input id="eta" type="text" class="form-control" name="eta" required autocomplete="eta">

                    @error('eta')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="date_received" class="col-md-4 col-form-label text-md-right">Date Received</label>

                <div class="col-md-6">
                    <input id="date_received" type="date" class="form-control @error('date_received') is-invalid @enderror" name="date_received" required autocomplete="date_received">

                    @error('date_received')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="pay_balance" class="col-md-4 col-form-label text-md-right">Pay Balance</label>

                <div class="col-md-6">
                    <input id="pay_balance" type="text" class="form-control @error('pay_balance') is-invalid @enderror" name="pay_balance"  autocomplete="pay_balance">

                    @error('pay_balance')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="delivery_status" class="col-md-4 col-form-label text-md-right">Delivery Status</label>

                <div class="col-md-6">
                    <input id="delivery_status" type="text" class="form-control" name="delivery_status" required autocomplete="delivery_status">

                    @error('delivery_status')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Add Container
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop