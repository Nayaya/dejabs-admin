@extends('layouts.app');

@section('content')

<div class="card">
    <div class="card-header card-primary">Edit Container [{{ $containers->container_no }}]</div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <br /> 
        @endif
        <form method="POST" action="{{ route('containers.update', $containers->container_no) }}">
                @method('PATCH') 
                @csrf
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-lg-3 col-form-label text-md-right">BL No.</label>
                            <div class="col-md-6">
                                <input id="bl_no" type="text" class="form-control" name="bl_no" value="{{ $containers->bl_no }}" readonly required>

                                @error('bl_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-lg-3 col-form-label text-md-right">Container No.</label>

                            <div class="col-md-6">
                                <input id="container_no" type="text" class="form-control @error('container_no') is-invalid @enderror" readonly name="container_no" value="{{ $containers->container_no}}" required autocomplete="container_no">

                                @error('container_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="size" class="col-md-3 col-lg-3 col-form-label text-md-right">Size</label>

                            <div class="col-md-6">
                                <input id="size" type="text" class="form-control @error('size') is-invalid @enderror" name="size" value="{{ $containers->size }}" required autocomplete="size">

                                @error('size')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="owner" class="col-md-3 col-lg-3 col-form-label text-md-right">Owner</label>

                            <div class="col-md-6">
                                <input id="owner" type="text" class="form-control" name="owner" value="{{ $containers->owner }}" required autocomplete="owner">

                                @error('owner')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="line" class="col-md-3 col-lg-3 col-form-label text-md-right">Line</label>

                            <div class="col-md-6">
                                <input id="line" type="text" class="form-control" name="line" value="{{ $containers->line }}" required autocomplete="line">

                                @error('line')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="line" class="col-md-3 col-lg-3 col-form-label text-md-right">Item (s)</label>

                            <div class="col-md-6">
                                <input id="items" type="text" class="form-control" name="items" value="{{ $containers->items }}" required autocomplete="items">

                                @error('items')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group row">
                            <label for="status" class="col-md-3 col-lg-3 col-form-label text-md-right">Status</label>

                            <div class="col-md-6">
                                <input id="status" type="text" class="form-control @error('status') is-invalid @enderror" name="status" value="{{ $containers->status }}" required autocomplete="status">

                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="eta" class="col-md-3 col-lg-3 col-form-label text-md-right">ETA</label>

                            <div class="col-md-6">
                                <input id="eta" type="text" class="form-control" name="eta" value="{{ $containers->eta }}" required autocomplete="eta">

                                @error('eta')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date_received" class="col-md-3 col-lg-3 col-form-label text-md-right">Date Received</label>

                            <div class="col-md-6">
                                <input id="date_received" type="date" class="form-control @error('date_received') is-invalid @enderror" name="date_received" value="{{ $containers->date_received }}" required autocomplete="date_received">

                                @error('date_received')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="delivery_status" class="col-md-3 col-lg-3 col-form-label text-md-right">Delivery Status</label>

                            <div class="col-md-6">
                                <input id="delivery_status" type="text" class="form-control" name="delivery_status" value="{{ $containers->delivery_status }}" required autocomplete="delivery_status">

                                @error('delivery_status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="payment_balance" class="col-md-3 col-lg-3 col-form-label text-md-right">Payment Balance</label>

                            <div class="col-md-6">
                                <input id="payment_balance" type="text" class="form-control" name="payment_balance" value="{{ $containers->pay_balance }}" autocomplete="payment_balance">

                                @error('payment_balance')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        </div>
                </div>
                <hr>
                <button type="button" class="btn btn-success btn-add-expense"><i class="fa fa-plus"></i> Add Expense</button>
                    <br><br>
                <div class="form-group row">
                    <div class="col-md-3 col-lg-3">
                        Item
                    </div>
                    <div class="col-md-3 col-lg-3">
                        Expenditure
                    </div>
                    <div class="col-md-3 col-lg-3">
                        Date
                    </div>
                    <div class="col-md-3 col-lg-3">
                        Payment Status
                    </div>
                </div>
                <div class="clonedInput form-group row" id="removeId1">
                    <div class="col-md-3 col-lg-3">
                         <select required="required" class="form-control" name="item_name[]">
                            @foreach ($expense_items as $val)
                            <option value="{{ $val }}">{{ $val }}</option>
                            @endforeach
                         </select>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <input id="item_expenditure" type="text" class="form-control @error('item_expenditure') is-invalid @enderror" name="item_expenditure[]" required autocomplete="item_expenditure">
                        @error('item_expenditure')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <input id="item_date" type="date" class="form-control @error('item_date') is-invalid @enderror" name="item_date[]" required autocomplete="item_date[]">
                        @error('item_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <input id="item_pay_status" type="text" class="form-control @error('item_pay_status') is-invalid @enderror" name="item_pay_status[]" required autocomplete="item_pay_status">
                        @error('item_pay_status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4 text-center">
                    <button type="submit" class="btn btn-success">
                        Update
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop