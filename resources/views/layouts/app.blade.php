<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('img/Logo.png') }}" alt="logo" height="22" width="22" style="margin-right: 4px">  {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li>
                            <a>
                               
                            </a>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            @can('user-create')
                            <li><a class="nav-link" href="{{ route('users.index') }}">Manage Users</a></li>
                            @endcan
                            @can('role-create')
                            <li><a class="nav-link" href="{{ route('roles.index') }}">Manage Role</a></li>
                            @endcan
                            <li><a class="nav-link" href="{{ route('containers.index') }}">Manage Containers</a></li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            @if(Session::has('success'))
                <div class="container">      
                    <div class="alert alert-success"><em> {!! session('success') !!}</em>
                    </div>
                </div>
            @endif 
            @if(Auth::check())
                   
             @endif
             <main class="py-4">
                @yield('content')
            </main>
           </div>
        </div>
<script type="text/javascript">
   $(document).ready(function() {
       let max_fields = 15;
       let counter = 1;    
       let inputFields = '<div class="col-md-3 col-lg-3">';
       inputFields += '<select required="required" class="form-control" name="item_name[]">';
       inputFields += '<option value="PAAR">PAAR</option>';
       inputFields += '<option value="SHIPPING">SHIPPING</option>';
       inputFields += '<option value="APM TERMINALS APP">APM TERMINALS APP</option>';
       inputFields += '<option value="CAPTURING">CAPTURING</option>';
       inputFields += '<option value="SGD">SGD</option>';
       inputFields += '<option value="DUTY">DUTY</option>';
       inputFields += '<option value="ICNL KD">ICNL KD</option>';
       inputFields += '<option value="DC CUSTOMS">DC CUSTOMS</option>';
       inputFields += '<option value="TRANSPORT">TRANSPORT</option>';
       inputFields += '<option value="SON">SON</option>';
       inputFields += '<option value="GT,RLS,LBR,LCK,AGNCY">GT,RLS,LBR,LCK,AGNCY</option>';
       inputFields += '<option value="STRIKE FORCE">STRIKE FORCE</option>';
       inputFields += '<option value="AGENCY">AGENCY</option>';
       inputFields += '<option value="OTHERS">OTHERS</option>';
       inputFields +=  '</select></div>';
       inputFields += '<div class="col-md-3 col-lg-3">';
       inputFields += '<input id="item_expenditure" type="text" class="form-control" name="item_expenditure[]" required autocomplete="item_expenditure">';
       inputFields += '</div>'
       inputFields += '<div class="col-md-3 col-lg-3"><input id="item_date" type="date" class="form-control" name="item_date[]" required autocomplete="item_date[]"></div>';
       inputFields += ' <div class="col-md-3 col-lg-3"><input id="item_pay_status" type="text" class="form-control" name="item_pay_status[]" required autocomplete="item_pay_status"></div>';
       
       $('.btn-add-expense').click(function(e){
            e.preventDefault();
            if(counter < max_fields){
                counter++;
                $('.clonedInput').append(inputFields)
            }
       })
   })
</script>
</body>
</html>
