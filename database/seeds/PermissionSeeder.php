<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions

        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'container-list',
            'container-create',
            'container-edit',
            'container-delete',
            'user-list',
            'user-create',  
            'user-edit',
            'user-delete'  
        ];
 
 
         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
       
        // create roles and assign existing permissions
        $operator= Role::create(['name' => 'user']);
        $operator->givePermissionTo('container-create');
        $operator->givePermissionTo('container-list');

        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo('container-edit');
        $admin->givePermissionTo('container-delete');
        $admin->givePermissionTo('container-list');
        $admin->givePermissionTo('container-create');

        $super_admin = Role::create(['name' => 'super-admin']);
        $super_admin->givePermissionTo('container-edit');
        $super_admin->givePermissionTo('container-delete');
        $super_admin->givePermissionTo('container-list');
        $super_admin->givePermissionTo('container-create');
        $super_admin->givePermissionTo('user-edit');
        $super_admin->givePermissionTo('user-delete');
        $super_admin->givePermissionTo('user-list');
        $super_admin->givePermissionTo('user-create');
        $super_admin->givePermissionTo('role-edit');
        $super_admin->givePermissionTo('role-delete');
        $super_admin->givePermissionTo('role-list');
        $super_admin->givePermissionTo('role-create');

        // Creating default Users
        $user = Factory(App\User::class)->create([
            'name' => 'User',
            'email' => 'user@dejabslogistics.com',
            'password' =>  Hash::make('operator@dejabs'),
            // factory default password is 'secret'
        ]);
        $user->assignRole($operator);

        $user = Factory(App\User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@dejabslogistics.com',
            'password' =>  Hash::make('admin@dejabs')
            // factory default password is 'secret'
        ]);
        $user->assignRole($admin);

        $user = Factory(App\User::class)->create([
            'name' => 'SuperAdmin',
            'email' => 'superadmin@dejabslogistics.com',
            'password' =>  Hash::make('superadmin@dejabs')
            // factory default password is 'secret'
        ]);
        $user->assignRole($super_admin);

    }
}
