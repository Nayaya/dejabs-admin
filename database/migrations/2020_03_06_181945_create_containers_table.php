<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('containers', function (Blueprint $table) {
            $table->string('container_no')->unique();
            $table->string('bl_no')->unique();
            $table->string('size');
            $table->string('owner');
            $table->string('status');
            $table->string('eta');
            $table->string('line');
            $table->string('items')->nullable(true);
            $table->integer('expense_id')->nullable(true);
            $table->string('pay_balance')->nullable(true);
            $table->string('delivery_status');
            $table->string('date_received');
            $table->string('created_by')->nullable(true);
            $table->string('updated_by')->nullable(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('containers');
    }
}
