<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Container;
use App\Expense;
use App\Item;
use Auth;
use Illuminate\Support\Facades\Input;

class ContainersController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:container-list|container-create|container-edit|container-delete', ['only' => ['index','show']]);
         $this->middleware('permission:container-create', ['only' => ['create','store']]);
         $this->middleware('permission:container-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:container-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.containers.index')->with('containers', Container::all());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.containers.create');
       
    }

    public function getClonedFields() {
        $input = Input::all();
        $id = $input['div_count'];
        $varId = 'removeId'. $id;
        $data = '
                <div class="clonedInput form-group row" id="'.$varId.'">
                <div class="col-md-3 col-lg-3">
                    <select required="required" id="item_name'. $varId .'" class="form-control" name="item_name[]">
                        @foreach ($expense_items as $key => $val)
                        <option value="{{ $key }}">{{ $val }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 col-lg-3">
                    <input id="item_name'. $varId .'" type="text" class="form-contro name="item_expenditure[]" required autocomplete="item_expenditure">
                </div>
                <div class="col-md-3 col-lg-3">
                    <input id="item_date'. $varId .'" type="date" class="form-control  name="item_date[]" required autocomplete="item_date[]">
                </div>
                <div class="col-md-3 col-lg-3">
                    <input id="item_pay_status'. $varId .'" type="text" class="form-control name="item_pay_status[]" required autocomplete="item_pay_status">
                </div>
                <div class="col-md-1">
                  <input type="button" class="btn btn-danger" name="del_item" value="Delete" onClick="removedClone('. $varId .');" />
                </div>
            </div>
        ';
        echo json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'bl_no' => 'required',
            'container_no' => 'required',
            'size' => 'required',
            'owner' => 'required',
            'status' => 'required',
            'eta' => 'required',
            'date_received' => 'required',
            'line' => 'required',
            'items' => 'required',
            'pay_balance' => '',
            'delivery_status' => 'required',
        ]);

       
        $container = Container::create([
            'bl_no' => $validatedData['bl_no'],
            'container_no' => $validatedData['container_no'],
            'size' => $validatedData['size'],
            'owner' => $validatedData['owner'],
            'status' => $validatedData['status'],
            'eta' => $validatedData['eta'],
            'date_received' => $validatedData['date_received'],
            'line' => $validatedData['line'],
            'pay_balance' => $validatedData['pay_balance'],
            'delivery_status' => $validatedData['delivery_status'],
            'items' => $validatedData['items'],
        ]);

        $expense = new Expense;
        $expense->container_id = $container->container_no;
        $expense->save();
        $container->expense_id = $expense->id;
        $container->save();

        return redirect('/admin/containers')->with('success', 'container added successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $containers = Container::find($id);
        $expenses =  Container::find($id)->expenses;
        $expense_items = ['PAAR', 'SHIPPING', 'APM TERMINALS APP', 'CAPTURING', 'SGD', 'DUTY', 'ICNL KD', 'DC CUSTOMS', 'TRANSPORT', 'SON', 'GT,RLS,LBR,LCK,AGNCY', 'STRIKE FORCE', 'AGENCY', 'OTHERS' ];

        return view('admin.containers.edit', compact(['containers', 'expenses', 'expense_items']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validatedData = $request->validate([
            'bl_no' => 'required',
            'container_no' => 'required',
            'size' => 'required',
            'owner' => 'required',
            'status' => 'required',
            'eta' => 'required',
            'date_received' => 'required',
            'line' => 'required',
            'items' => 'required',
            'payment_balance' => '',
            'delivery_status' => 'required',
            'item'
        ]);

        $container = Container::find($id);
        $container->bl_no = $validatedData['bl_no'];
        $container->container_no = $validatedData['container_no'];
        $container->size = $validatedData['size'];
        $container->owner = $validatedData['owner'];
        $container->status = $validatedData['status'];
        $container->eta = $validatedData['eta'];
        $container->date_received = $validatedData['date_received'];
        $container->line = $validatedData['line'];
        $container->pay_balance = $validatedData['payment_balance'];
        $container->delivery_status = $validatedData['delivery_status'];
        $container->items = $validatedData['items'];
        $container->save();

        for($i=0; $i<count($request['item_name']); $i++) {
            $data['item_name'] = $request['item_name'][$i];
            $data['item_expenditure'] = $request['item_expenditure'][$i];
            $data['item_date'] = $request['item_date'][$i];
            $data['item_pay_status'] = $request['item_pay_status'][$i];
            $expense = new Expense;
            $expense->container_id = $container->container_no;
            $expense->item =  $data['item_name'];
            $expense->expenditure = $data['item_expenditure'];
            $expense->date =  $data['item_date'];
            $expense->payment_status =  $data['item_pay_status'];
            $expense->save();
        }

        return redirect('/admin/containers')->with('success', 'container Updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $container = Container::findOrFail($id);
        $container->delete();

        return redirect('/admin/containers')->with('success', 'container deleted successfully');
    }
}
