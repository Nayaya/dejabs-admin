<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    //

    public function containers()
    {
        return $this->belongsTo('App\Container');
    }
}
