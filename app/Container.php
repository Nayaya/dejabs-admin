<?php

namespace App;

use Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Container extends Model
{
    //

    use SoftDeletes;

    protected $fillable = [
        'bl_no',
        'container_no',
        'size',
        'owner',
        'status',
        'eta',
        'date_received',
        'line',
        'items',
        'pay_balance',
        'delivery_status',

    ];

    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'container_no';
    protected $dates = ['deleted_at'];

    public static function boot()
    {
        parent::boot();
        static::creating(function($container)
        {
            $container->created_by = Auth::user()->id;
            $container->updated_by = Auth::user()->id;
        });
        

        static::updating(function($container)
        {
            $container->updated_by = Auth::user()->id;
        });
    }

    public function expenses()
    {
        return $this->hasMany('App\Expense', 'container_id', 'container_no');
    }
}
